w1retap in Debian
-----------------
The original default configuration file for the w1retap daemon
(/etc/default/w1retap) is reserved for init system settings. Therefore the
source is patched to use /etc/w1retap.conf as the default instead. Note that
the environment variable W1RCFILE and the location ~/.config/w1retap/rc can
still be used.

The configuration file (/etc/w1retap.conf) will need to be locally modified to
choose which adapter to use, set where to get the list of sensors to monitor
and set where to record the logged data. A sample configuration file is
installed and is configured to read from a USB DS2490-1 adapter, use a text
based list of sensors (/etc/w1retap-sensors.dat) and write data to the default
text file (/var/lib/w1retap/w1retap.log). This should be changed to suit local
needs.

The w1find utility can scan an adapter for sensors and list them. The w1sensors
script is included to help with sensor list configuration, it can output a
sensor list in text or SQL format. The output of the utilities helps build the
sensors list and can be either be piped to /etc/w1retap-sensors.dat or the
configured database command. An example run:

$ sudo w1find DS2490-1 | w1sensors -f
2885848F050000BD|DS1820|TMP_1|Temperature #1|°C|||||

$ sudo w1find DS2490-1 | w1sensors
INSERT into w1sensors values (1,'2885848F050000BD','DS1820','TMP_1','Temperature #1','°C',NULL,NULL,NULL,NULL,NULL);

The package creates a new user (w1retap) for the daemon to run as and this user
is a member of the dialout group because serial devices (eg /dev/ttyS0) are
group owned by dialout. Thus the daemon is able to read and write to the
adapter. When USB adapters are used libusb communicates with the adapters. A
udev rule is installed to make 1wire USB devices in /dev/bus/usb/* group owned
by dialout.  This rule will only take effect after the package is installed, so
the adapter may need to be plugged out and in again. The lsusb utility can
show the device IDs that are used to create rules:
$ lsusb | grep DS1490F
Bus 003 Device 002: ID 04fa:2490 Dallas Semiconductor DS1490F 2-in-1 Fob, 1-Wire adapter
$
Extra udev rules can be placed in /etc/udev/rules.d/ using different USB vendor
and product numbers. Please file bugs to add more 1-Wire USB adapters rules. A
sample udev rule (eg /etc/udev/rules.d/w1retap.rules):
ACTION=="add", SUBSYSTEM=="usb", ATTRS{idVendor}=="04fa", ATTRS{idProduct}=="2490", GROUP="dialout"

When installed w1retap does not start and does not start when the system boots.
Once the configuration is completed it can be set to start on boot
automatically depending on the init system that is in use. If sysvinit is used
enable the init script and then start with "sudo service w1retap start". If
systemd is in use enable the service with "sudo systemctl enable w1retap" and
start it with "sudo systemctl start w1retap".

If using sysvinit the arguments to the daemon can be changed by editing
/etc/default/w1retap and changing DAEMON_ARGS. If using systemd then create
/etc/systemd/system/w1retap.service.d/local.conf with the following:
[Service]
ExecStart=
ExecStart=/usr/bin/w1retap -d -R -t 120

Storing the data in PostgreSQL is straight forward, uncomment the "log =
w1pgsql" in /etc/w1retap.conf. Then install the PostgreSQL, create a user,
create a database and populate the database:

$ sudo apt-get install postgresql
$ sudo -u postgres createuser w1retap
$ sudo -u postgres createdb -O w1retap w1retap
$ cat /usr/share/doc/w1retap-doc/mksenst.sql | sudo -u w1retap psql

If using any of the available database backends (mysql, postgresql) then this
backend will need to be started before the w1retap daemon starts, to achieve
this extend or create /etc/systemd/system/w1retap.service.d/local.conf with the
following and substitute in the relevant backend:
[Unit]
Requires=postgresql.service
After=postgresql.service

Another option is to recreate an entire unit file in
/etc/systemd/system/w1retap.service. This can also be used to run multiple
w1retap instances by using Environment and setting W1RCFILE.

 -- Thomas Stewart <thomas@stewarts.org.uk>  Fri, 16 Nov 2016 22:45:13 +0000
