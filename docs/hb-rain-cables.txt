When we put together the Rain Gauge it is hooked up as follows: The
reed switch goes between B and +5v

The cable coming into the Rain Gauge has a copper and a silver
wire. The copper goes to DQ and the silver goes to GND.

Let me know if that helped.

Eric
